package br.com.pablopes.thecatbreeds.core.security.service;


import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.pablopes.thecatbreeds.domain.model.User;
import br.com.pablopes.thecatbreeds.domain.repository.UserRepository;

@Service
public class AppUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UserRepository usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> usuarioOptional = usuarioRepository.findByEmail(email);
		User usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Usuário e/ou Senha incorretos!!!"));
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}
	
	private Collection<? extends GrantedAuthority> getPermissoes(User usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		usuario.getGroup().getPermissions()
			.forEach(
					p -> authorities.add(new SimpleGrantedAuthority(p.getDescription().toUpperCase())
			)
		);
		return authorities;
	}
}