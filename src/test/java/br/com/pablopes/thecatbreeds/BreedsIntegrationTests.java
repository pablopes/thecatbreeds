package br.com.pablopes.thecatbreeds;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.pablopes.thecatbreeds.domain.model.Breed;
import br.com.pablopes.thecatbreeds.domain.model.Image;
import br.com.pablopes.thecatbreeds.domain.model.Weight;
import br.com.pablopes.thecatbreeds.domain.service.BreedService;

@SpringBootTest
public class BreedsIntegrationTests {
	@Autowired private BreedService service;

	@Test
	public void testInsertBreed_WhenBreedOk_ThenSuccess() {
		Image image = new Image();
		Weight weight = new Weight();
		Breed newBreed = new Breed();

		image.setHeight(1);
		image.setId("123");
		image.setUrl("www.xuz.com.br");
		image.setWidth(1);

		weight.setImperial("teste");
		weight.setMetric("teste");

		newBreed.setWeight(weight);
		newBreed.setImage(image);

		newBreed.setAdaptability(1);
		newBreed.setAffection_level(1);
		newBreed.setAlt_names("teste");

		newBreed = service.save(newBreed);

		assertThat(newBreed).isNotNull();
		assertThat(newBreed.getCode()).isNotNull();
		assertThat(newBreed.getImage()).isNotNull();
		assertThat(newBreed.getWeight()).isNotNull();
	}
	
	@Test
	public void testGetBreed_WhenAll_ThenSuccess() {
		List<Breed> breeds = service.findAll();
		
		assertThat(breeds).isNotNull();
	}
	
	@Test
	public void testGetBreed_WhenForName_ThenSuccess() throws InterruptedException {
		List<Breed> breeds = service.findByName("ab");
		
		assertThat(breeds).isNotNull();
	}
}
