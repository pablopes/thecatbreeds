package br.com.pablopes.thecatbreeds.api.exceptionHandler;

import lombok.Getter;

@Getter
public enum ProblemType {
	DADOS_INVALIDOS("/dados-invalidos", "Dados inválidos"),
	ENTIDADE_EM_USO("/entidade-em-uso", "Entidade Em Uso."),
	ERRO_DE_SISTEMA("/erro-de-sistema", "Erro de Sistema"),
	ERRO_NEGOCIO("/erro-negocio", "Violação de Regra de Negócio"),
	MENSAGEM_NAO_COMPREENDIDA("/mensagem-nao-compreendida", "Mensagem Não Compreendida"),
	RECURSO_NAO_ENCONTRADO("/recurso-nao-encontrado", "Recurso Não Encontrado."),
	PARAMETRO_INVALIDO("/parametro-invalido", "Parâmetro Inválido");
	
	private String title;
	private String uri;
	
	ProblemType(String path, String title){
		this.uri = "https://localhost.com.br" + path;
		this.title = title;
	}
}
