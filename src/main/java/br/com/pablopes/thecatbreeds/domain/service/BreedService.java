package br.com.pablopes.thecatbreeds.domain.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.pablopes.thecatbreeds.domain.exception.BreedNaoEncontradaException;
import br.com.pablopes.thecatbreeds.domain.exception.EntidadeEmUsoException;
import br.com.pablopes.thecatbreeds.domain.exception.NegocioException;
import br.com.pablopes.thecatbreeds.domain.model.Breed;
import br.com.pablopes.thecatbreeds.domain.model.Image;
import br.com.pablopes.thecatbreeds.domain.model.Weight;
import br.com.pablopes.thecatbreeds.domain.repository.BreedRepository;
import br.com.pablopes.thecatbreeds.infrastructure.repository.spec.BreedForNameSpec;

@Service
public class BreedService {
	private static final String MSG_BREED_IN_USE = "Breed de código %d não pode ser removida, pois está em uso!!!";
	
	@Autowired private BreedRepository repository;
	@Autowired private RestTemplate restTemplate;
	
	public List<Breed> findAll(){
		if(repository.count() <= 0) {
			String json = readAPI("https://api.thecatapi.com/v1/breeds");
			return desserialized(json);		
		}
		
		return repository.findAll();
	}
	@Cacheable("breeds")
	public List<Breed> findByName(String breedId) throws InterruptedException {
		List<Breed> breeds = repository.findAll(BreedForNameSpec.comNomeSemelhante(breedId));
		
		if(breeds.isEmpty()) {
			String json = readAPI("https://api.thecatapi.com/v1/breeds/search?q=" + breedId);
			return desserialized(json);
		}
		
		return breeds;
		
	}
	public Breed findByCode(int breedId) {
		return repository.findByCode(breedId)
				.orElseThrow(() -> new BreedNaoEncontradaException(breedId));
	}
	@Transactional
	public Breed save(Breed breed) {	
		return repository.save(breed);
	}
	@Transactional
	public void delete(int breedId) {
		try {
			repository.deleteById(breedId);
		} catch (EmptyResultDataAccessException ex) {
			throw new BreedNaoEncontradaException(breedId);
		} catch (DataIntegrityViolationException ex) {
			throw new EntidadeEmUsoException(String.format(MSG_BREED_IN_USE, breedId));
		}
	}
	
	private String readAPI(String link ) {
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-api-key", "0fbbad83-eb7c-4a8f-b325-cec61062065c");
	    
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    
	    return restTemplate.exchange(link, HttpMethod.GET, entity, String.class).getBody();
	}
	
	private List<Breed> desserialized(String json) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Breed[] breeds;
		
		List<Breed> breedsReturn = new ArrayList<Breed>();
		try {
			breeds = mapper.readValue(json, Breed[].class);
			
			for(Breed breed: breeds) {
				if(breed.getImage() == null) {
					breed.setImage(new Image());
				}
				if(breed.getWeight() == null) {
					breed.setWeight(new Weight());
				}
				breedsReturn.add(repository.save(breed));
			}
			
			return breedsReturn;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new NegocioException("Erro ao mapear Json");			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new NegocioException("Erro ao Processar Json");			
		}
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

	
}
