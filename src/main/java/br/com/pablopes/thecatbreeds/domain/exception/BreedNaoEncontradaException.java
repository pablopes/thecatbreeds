package br.com.pablopes.thecatbreeds.domain.exception;

public class BreedNaoEncontradaException extends EntidadeNaoEncontradaException {
	private static final long serialVersionUID = 1L;
	
	public BreedNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
	
	public BreedNaoEncontradaException(int breedID) {
		this(String.format("Não existe um cadastro de breed com o código %d", breedID));
	}
}