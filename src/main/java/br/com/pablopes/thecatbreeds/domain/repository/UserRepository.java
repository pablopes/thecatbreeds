package br.com.pablopes.thecatbreeds.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import br.com.pablopes.thecatbreeds.domain.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public Optional<User> findByEmail(String email);
}