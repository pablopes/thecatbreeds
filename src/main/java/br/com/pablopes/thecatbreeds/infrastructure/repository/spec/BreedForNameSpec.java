package br.com.pablopes.thecatbreeds.infrastructure.repository.spec;

import org.springframework.data.jpa.domain.Specification;

import br.com.pablopes.thecatbreeds.domain.model.Breed;

public class BreedForNameSpec {
	public static Specification<Breed> comNomeSemelhante(String name) {
		return (root, query, builder) -> builder.like(builder.upper(root.get("name")),
				"%" + name.toUpperCase() + "%");
	}
}
