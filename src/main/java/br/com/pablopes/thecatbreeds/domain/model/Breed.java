package br.com.pablopes.thecatbreeds.domain.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="breeds")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Breed {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private int code;
	private String id;
	private String name;
	private String cfa_url;
	private String vetstreet_url;
	private String vcahospitals_url;
	private String temperament;
	private String origin;
	private String country_codes;
	private String country_code;
	private String description;
	private String life_span;
	private int indoor;
	private int lap;
	private String alt_names;
	private int adaptability;
	private int affection_level;
	private int child_friendly;
	private int dog_friendly;
	private int energy_level;
	private int grooming;
	private int health_issues;
	private int intelligence;
	private int shedding_level;
	private int social_needs;
	private int stranger_friendly;
	private int vocalisation;
	private int experimental;
	private int hairless;
	@Column(name = "naturals")
	private int natural;
	private int rare;
	private int rex;
	private int suppressed_tail;
	private int short_legs;
	private String wikipedia_url;
	private int  hypoallergenic;
	private String reference_image_id;
	@OneToOne(cascade = CascadeType.ALL)
	private Image image;
	@OneToOne(cascade = CascadeType.ALL)
	private Weight weight;
}
