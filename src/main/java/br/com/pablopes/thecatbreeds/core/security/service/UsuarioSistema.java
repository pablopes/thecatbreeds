package br.com.pablopes.thecatbreeds.core.security.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import br.com.pablopes.thecatbreeds.domain.model.User;

public class UsuarioSistema extends org.springframework.security.core.userdetails.User{
	private static final long serialVersionUID = 1L;
	
	private User usuario;
	
	public UsuarioSistema(User  usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getEmail(), usuario.getPass(), authorities);
		this.usuario = usuario;
	}
	
	public User getUsuario() {
		return usuario;
	}
}
