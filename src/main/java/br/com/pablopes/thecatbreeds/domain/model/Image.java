package br.com.pablopes.thecatbreeds.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="image")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Image {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private int code;
	private String id;
	private int width;
	private int height;
	private String url;
}
