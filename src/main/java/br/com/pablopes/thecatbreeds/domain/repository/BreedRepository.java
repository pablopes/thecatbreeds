package br.com.pablopes.thecatbreeds.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.pablopes.thecatbreeds.domain.model.Breed;

public interface BreedRepository extends JpaRepository<Breed, Integer>, JpaSpecificationExecutor<Breed> {
	public List<Breed> findById(String breedId);
	public Optional<Breed> findByCode(int breedCode);
}