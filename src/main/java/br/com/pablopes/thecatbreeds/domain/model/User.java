package br.com.pablopes.thecatbreeds.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="users")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private int code;
	@NotNull
	@Size(max = 50)
	private String name;
	@Size(max = 150)
	private String pass;
	@Size(max = 150)
	private String email;
	
	@ManyToOne
	@JoinColumn(name="group_code")
	private Group group;
}