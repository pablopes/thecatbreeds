package br.com.pablopes.thecatbreeds.domain.model;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="groups")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Group {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private int code;
	@Size
	@NotNull
	private String description;
	@Valid
	@NotNull
	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(
			name = "group_permission", 
			joinColumns = @JoinColumn(name = "group_code"),
			inverseJoinColumns = @JoinColumn(name = "permission_code")
	)
	private List<Permission> permissions;
}

