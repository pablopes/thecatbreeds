package br.com.pablopes.thecatbreeds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ThecatbreedsApplication {
	public static void main(String[] args) {
		SpringApplication.run(ThecatbreedsApplication.class, args);
	}
}
