package br.com.pablopes.thecatbreeds.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.pablopes.thecatbreeds.domain.model.Breed;
import br.com.pablopes.thecatbreeds.domain.service.BreedService;

@RestController
@RequestMapping("/breeds")
public class BreedResource {
	@Autowired private BreedService service;
	
	@GetMapping
	public List<Breed> findAll(){
		return service.findAll();
	}
	
	@GetMapping("/search")
	public List<Breed> findByName(String name) throws InterruptedException {
		return service.findByName(name);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Breed save(@RequestBody @Valid Breed breed) {
		return service.save(breed);
	}

	@PutMapping("/{breedId}")
	public Breed update(@PathVariable int breedId, @RequestBody @Valid Breed breed) {
		Breed breedAual = service.findByCode(breedId);
		BeanUtils.copyProperties(breed, breedAual, "id");
		return service.save(breedAual);
	}

	@DeleteMapping("/{breedId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int breedId) {
		service.delete(breedId);
	}
}
