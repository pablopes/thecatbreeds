package br.com.pablopes.thecatbreeds;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Gerador {
	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String password = "thecatbreeds";
		
		System.out.println(encoder.encode(password));	
	}
}
