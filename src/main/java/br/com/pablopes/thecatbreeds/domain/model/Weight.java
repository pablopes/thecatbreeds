package br.com.pablopes.thecatbreeds.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="weight")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Weight {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private int code;
	private String imperial;
	private String metric;
}
