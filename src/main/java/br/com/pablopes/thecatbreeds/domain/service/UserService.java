package br.com.pablopes.thecatbreeds.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.pablopes.thecatbreeds.domain.model.Group;
import br.com.pablopes.thecatbreeds.domain.model.User;
import br.com.pablopes.thecatbreeds.domain.repository.UserRepository;

@Service
public class UserService {
	@Autowired private UserRepository repository;

	@Transactional
	public User save(User user) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPass(encoder.encode(user.getPass()));
		
		Group group = new Group();
		group.setCode(2);
		user.setGroup(group);
		return repository.save(user);
	}
}