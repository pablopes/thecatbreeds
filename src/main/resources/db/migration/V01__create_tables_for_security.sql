﻿/* ##################################################
   ############### GERANDO AS TABELAS ###############
   ##################################################*/

CREATE TABLE PERMISSION (
   	CODE		SERIAL,
   	DESCRIPTION	VARCHAR(50)	NOT NULL,
   	CONSTRAINT PERMISSION_CODE_PK PRIMARY KEY(CODE)
);
   
CREATE TABLE GROUPS (
   	CODE			SERIAL,
   	DESCRIPTION	VARCHAR(50) NOT NULL,
   	CONSTRAINT GROUP_CODE_PK PRIMARY KEY(CODE)
);
 
CREATE TABLE GROUP_PERMISSION (
   	GROUP_CODE		INT NOT NULL,
   	PERMISSION_CODE	INT	NOT NULL,
   	PRIMARY KEY (GROUP_CODE, PERMISSION_CODE)
);
   
CREATE TABLE USERS (
   	CODE		SERIAL,
   	NAME		VARCHAR(50),
   	PASS		VARCHAR(150),
   	EMAIL		VARCHAR(150) UNIQUE ,
	GROUP_CODE	INT NOT NULL,
   	CONSTRAINT USER_CODE_PK PRIMARY KEY(CODE),
   	CONSTRAINT GROUP_CODE_FK FOREIGN KEY (GROUP_CODE) REFERENCES GROUPS(CODE)
);